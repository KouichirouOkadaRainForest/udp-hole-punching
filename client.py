#!/usr/bin/env python
"""UDP hole punching client."""
from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

import sys


class ClientProtocol(DatagramProtocol):
    def startProtocol(self):
        """Register with the rendezvous server."""
        self.server_connect = False
        self.peer_init = False
        self.peer_connect = False
        self.peer_address = None
        #self.transport.write('0', (sys.argv[1], int(sys.argv[2])))
        self.transport.write("NAME:"+sys.argv[1], (sys.argv[2], int(sys.argv[3])))

    def toAddress(self, data):
        """Return an IPv4 address tuple."""
        ip, port = data.split(':')
        return (ip, int(port))

    def datagramReceived(self, datagram, host):
        if "Key" == datagram :
            strip = raw_input('IP:PORT or next >>')
            if strip != "next" :
                s_address = self.toAddress(strip)
                print(s_address)
                #self.transport.write('connect', s_address)
                msg = raw_input('message >>')
                self.transport.write("MSG:"+msg, s_address)
        elif "MSG" in datagram :
            print(datagram)
            strip = raw_input('IP:PORT or next >>')
            if strip != "next" :
                s_address = self.toAddress(strip)
                print(s_address)
                #self.transport.write('connect', s_address)
                msg = raw_input('message >>')
                self.transport.write("MSG:"+msg, s_address)
        else :
            print(datagram)
        return

if __name__ == '__main__':
    if len(sys.argv) < 4:
        print "Usage: ./client NAME RENDEZVOUS_IP RENDEZVOUS_PORT"
        sys.exit(1)

    protocol = ClientProtocol()
    t = reactor.listenUDP(0, protocol)
    reactor.run()
