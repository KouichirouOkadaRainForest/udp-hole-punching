#!/usr/bin/env python

from twisted.internet.protocol import DatagramProtocol
from twisted.internet import reactor

import sys


class ServerProtocol(DatagramProtocol):
    client_list = {}

    def __init__(self):
        """Initialize with empy address list."""
        self.addresses = []

    def addressString(self, address):
        """Return a string representation of an address."""
        ip, port = address
        return ':'.join([ip, str(port)])

    def datagramReceived(self, datagram, address):
        """Handle incoming datagram messages."""
        #if datagram == '0':
        if "NAME" in datagram :
            msg_0 = self.addressString(address)

            self.client_list[msg_0] = "[%s] %s"%(datagram,msg_0) 
            print 'Registration from %s:%d' % address
            self.addresses.append(address)

            self.transport.write("My Info", address)
            self.transport.write(self.client_list[msg_0], address)

            for user_address in self.addresses :
                self.transport.write("User Address List", user_address)
            for user_address in self.addresses :
                for send_address in self.addresses :
                    msg_1 = self.addressString(user_address)
                    if send_address != user_address :
                        self.transport.write(self.client_list[msg_1], send_address)
            for user_address in self.addresses :
                self.transport.write("Key", user_address)

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print "Usage: ./server.py PORT"
        sys.exit(1)

    port = int(sys.argv[1])
    reactor.listenUDP(port, ServerProtocol())
    print 'Listening on *:%d' % (port)
    reactor.run()
